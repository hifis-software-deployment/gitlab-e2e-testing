<!--
SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: Apache-2.0
-->

# GitLab End-to-End (E2E) Testing
This project is used to automatically test functionality of the HIFIS Software
GitLab service.
In the future, this project will be generalized in a way that it is
generically useful also for other GitLab installations.

## Run the Tests Locally
This project uses Selenium to test the Web UI of GitLab.
The easiest is to use Docker/Podman to make Selenium locally available.
Run the container with this command.

```bash
docker run -d -p 4444:4444 -p 7900:7900 --shm-size="2g" selenium/standalone-chrome:4.1.2-20220217
```

The project dependencies are managed via [Poetry](https://python-poetry.org/).
Please refer to the
[installation instructions](https://python-poetry.org/docs/#installation)
of Poetry for a guide on how to install Poetry.

Once available, the dependencies can be installed via this command.

```bash
poetry install
```

Run the tests with the following command.
Choose one of the provided configuration files:

* `pytest.staging.ini` - For use with `scm.hzdr.de`
* `pytest.production.ini` - For use with `gitlab.hzdr.de`

Example call for `scm.hzdr.de`:

```bash
poetry run pytest -c pytest.staging.ini tests/
```
