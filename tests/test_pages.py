# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test GitLab Pages."""

from conftest import log_in_into_helmholtz_aai
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


def test_access_pages(browser_ignore_cert_errors: WebDriver, config):
    """Check that GitLab Pages works."""
    # set variables used
    pages_url: str = config.getini("pages_url")
    namespace: str = config.getini("namespace")
    private_project_path: str = config.getini("private_project_path")
    project_pages_url: str = (
        "https://"
        + namespace
        + "."
        + pages_url
        + "/"
        + private_project_path.split("/")[1]
        + "/"
    )

    # GET request to load URL
    browser_ignore_cert_errors.get(project_pages_url)

    # logging in into Helmholtz AAI given credentials and GitHub login method
    log_in_into_helmholtz_aai(
        request_config=config, webdriver_browser=browser_ignore_cert_errors
    )

    # wait until page is loaded and headline tag on page is found
    WebDriverWait(browser_ignore_cert_errors, 20).until(
        ec.presence_of_element_located(
            (By.XPATH, "//h1[contains(text(),'GitLab Pages')]")
        )
    )

    # assert that correct URL is loaded
    assert (
        browser_ignore_cert_errors.current_url == project_pages_url
    ), "Browser is not at the expexcted URL after logging in via Helmholtz AAI."
