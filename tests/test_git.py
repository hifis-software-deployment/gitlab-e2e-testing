# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test interaction via Git."""

import datetime
import os
from urllib.parse import urljoin

import pygit2


def test_clone_https_public(config, tmp_path):
    """Check if a public repository can be cloned via HTTPS."""
    repo_url: str = urljoin(
        config.getini("gitlab_url"), f"{ config.getini('public_project_path') }.git"
    )
    repo_path: str = os.path.join(tmp_path, "repository")

    pygit2.clone_repository(repo_url, repo_path)

    assert os.path.exists(
        os.path.join(repo_path, ".git")
    ), "Directory does not contain a folder .git. Cloning a repository failed."


def test_clone_https_private(config, tmp_path, credential_callback):
    """Check if a private repository can be cloned via HTTPS."""
    repo_url: str = urljoin(
        config.getini("gitlab_url"), f"{ config.getini('private_project_path') }.git"
    )
    repo_path: str = os.path.join(tmp_path, "repository")

    pygit2.clone_repository(repo_url, repo_path, callbacks=credential_callback)

    assert os.path.exists(
        os.path.join(repo_path, ".git")
    ), "Directory does not contain a folder .git. Cloning a repository failed."


def test_push_and_delete_branch(config, tmp_path, credential_callback):
    """Check that branches can be pushed and deleted."""
    # define all variables needed
    current_datetime: str = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    username: str = config.getini("gitlab_username")
    email: str = username + "@temp.mailbox.org"
    repo_url: str = urljoin(
        config.getini("gitlab_url"), f"{ config.getini('private_project_path') }.git"
    )
    repo_path: str = os.path.join(tmp_path, "repository")
    filename_only: str = "e2e-test-file-" + current_datetime + ".md"
    filepath: str = repo_path + "/" + filename_only
    commit_message: str = (
        "E2e-test-commit of file " + filename_only + " on " + current_datetime
    )
    file_content: str = (
        "E2e-test-text in file " + filename_only + " on " + current_datetime
    )
    remote_name: str = "origin"
    branch_name: str = "e2e-test-branch-" + current_datetime
    branch_name_origin: str = remote_name + "/" + branch_name

    # clone repository
    cloned_repo = pygit2.clone_repository(
        repo_url, repo_path, callbacks=credential_callback
    )

    # create branch
    cloned_repo.create_branch(branch_name, cloned_repo.head.peel())

    # checkout new branch
    branch = cloned_repo.lookup_branch(branch_name)
    ref = cloned_repo.lookup_reference(branch.name)
    cloned_repo.checkout(ref)

    # prepare new commit of a new file
    with open(filepath, "w") as file:
        file.write(file_content)

    # make commit
    index = cloned_repo.index
    index.add(filename_only)
    index.write()
    author = pygit2.Signature(username, email)
    committer = pygit2.Signature(username, email)
    tree = index.write_tree()
    reference = cloned_repo.head.name
    parents = [cloned_repo.head.target]
    cloned_repo.create_commit(
        reference, author, committer, commit_message, tree, parents
    )

    # get remote origin
    remote = cloned_repo.remotes[remote_name]

    # push new branch to remote origin
    remote.push(["+refs/heads/" + branch_name], callbacks=credential_callback)
    # fetch from remote origin
    remote.fetch(callbacks=credential_callback)
    # get list of remote branches
    remote_branches: list = list(cloned_repo.branches.remote)

    # 1: assert that new branch is in the list of remote branches
    assert branch_name_origin in remote_branches, (
        "Branch " + branch_name + " not found in remote repository."
    )

    # delete remote branch
    remote.push([":refs/heads/" + branch_name], callbacks=credential_callback)
    # fetch from remote origin
    remote.fetch(callbacks=credential_callback)
    # get list of remote branches
    remote_branches: list = list(cloned_repo.branches.remote)

    # 2: assert that new branch is not anymore in the list of remote branches
    assert branch_name_origin not in remote_branches, (
        "Branch " + branch_name + " still found in remote repository."
    )
