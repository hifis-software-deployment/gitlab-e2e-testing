# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Demo app to test GitLab Package Registry."""


def hello():
    """Say hello."""
    print("hello")


if __name__ == "__main__":
    hello()
