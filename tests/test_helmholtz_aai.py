# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test the GitLab login via Helmholtz AAI."""

from conftest import log_in_into_helmholtz_aai
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def test_helmholtz_aai_login(browser, config):
    """Try out the login process via Helmholtz AAI."""
    gitlab_url: str = config.getini("gitlab_url")
    expected_gitlab_username: str = config.getini("gitlab_username")

    browser.get(gitlab_url)

    # logging in into Helmholtz AAI given credentials and GitHub login method
    log_in_into_helmholtz_aai(request_config=config, webdriver_browser=browser)

    # Check that the "+"-button is present
    WebDriverWait(browser, 20).until(
        EC.presence_of_element_located((By.ID, "create-menu-toggle"))
    )

    assert (
        browser.current_url == gitlab_url
    ), "Browser is not at the expexcted URL after logging in via Helmholtz AAI."

    assert (
        expected_gitlab_username in browser.page_source
    ), "Make sure that user is logged in by looking for the username."
