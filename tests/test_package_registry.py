# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test GitLab Package Registry."""

import datetime
import os
import re
from pathlib import Path

import pytest
import toml
from cleo.io.buffered_io import BufferedIO
from poetry.console.commands.build import BuildCommand
from poetry.factory import Factory
from poetry.installation.installer import Installer
from poetry.poetry import Poetry
from poetry.publishing.publisher import Publisher
from poetry.utils.env import EnvManager


@pytest.mark.package_registry
def test_gitlab_package_registry_publishing(config):
    """Check that package can be published in GitLab Package Registry."""
    # declare variables needed
    project_path: str = os.getcwd() + "/tests/package_registry_app/"
    filepath_template: str = project_path + "pyproject-template.toml"
    filepath: str = project_path + "pyproject.toml"
    current_datetime: str = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    version_pattern = re.compile(r"app-0.1.0.post" + current_datetime)

    # replace placeholder PACKAGE_VERSION_DATETIME by current date-time
    toml_content = toml.load(filepath_template)
    with open(filepath, "w") as toml_file:
        toml_content["tool"]["poetry"]["version"] = toml_content["tool"]["poetry"][
            "version"
        ].replace("PACKAGE_VERSION_DATETIME", current_datetime)
        toml.dump(toml_content, toml_file)

    # init Poetry
    io = BufferedIO()
    poetry = Factory().create_poetry(project_path, True, io, False, False)

    # create a virtual environment
    env_man = EnvManager(poetry, io)
    venv = env_man.activate("3")

    # install Python dependencies
    installer = Installer(
        io,
        venv,
        poetry.package,
        poetry.locker,
        poetry.pool,
        poetry.config,
    )
    installer.update(True)
    installer.run()

    # build Python package "app"
    build_cmd: BuildCommand = BuildCommand()
    build_cmd.set_poetry(poetry)
    build_cmd.set_env(venv)
    build_cmd.run(io)

    # adapt Poetry configuration to include GitLab Package Registry
    registries: dict = poetry.config.config["repositories"]
    gitlab: dict = {
        "gitlab": {
            "url": config.getini("gitlab_url")
            + "api/v4/projects/"
            + config.getini("private_project_id")
            + "/packages/pypi"
        }
    }
    gitlab_registry: dict = {**registries, **gitlab}
    poetry.config.config.__setitem__("repositories", gitlab_registry)
    poetry = poetry.set_config(poetry.config)

    # publish Python package "app" to GitLab Package Registry
    publisher: Publisher = Publisher(poetry, io)
    publisher.publish(
        "gitlab", "package_deploy_user", config.getoption("package_deploy_token")
    )

    # Install latest Python package "app" from GitLab package Registry
    venv.run_pip(
        "install",
        "app",
        "-t",
        project_path,
        "--index-url",
        "https://package_deploy_user:"
        + config.getoption("package_deploy_token")
        + "@"
        + config.getini("gitlab_domain")
        + "api/v4/projects/"
        + config.getini("private_project_id")
        + "/packages/pypi/simple",
    )

    # get list of all directories of locally built python packages
    python_packages_list = ",".join(
        [os.path.basename(os.path.normpath(x[0])) for x in os.walk(project_path)]
    )

    # do pattern matching
    search_result = version_pattern.search(python_packages_list)

    # assert that the correct version of the Python package is now installed
    assert search_result is not None, (
        "version pattern '"
        + version_pattern.pattern
        + "' is not contained in: "
        + python_packages_list
    )
