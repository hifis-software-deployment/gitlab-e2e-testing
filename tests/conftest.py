# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Pytest configuration."""

import os

import pygit2
import pytest
from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


def pytest_addoption(parser):
    """Add additional configuration to be retrieved via the config fixture."""
    parser.addini(
        "gitlab_url",
        "The GitLab URL to run the test with.",
        default="https://scm.hzdr.de",
    )
    parser.addini(
        "gitlab_domain",
        "The GitLab domain without protocol.",
        default="scm.hzdr.de",
    )
    parser.addini(
        "registry_url",
        "The GitLab Container Registry URL.",
        default="registry-149-220-142-59.nip.io",
    )
    parser.addini(
        "namespace",
        "The namespace of the project with the GitLab Pages.",
        default="software-helmholtz-codebase",
    )
    parser.addini(
        "pages_url",
        "The GitLab Pages URL.",
        default="pages-149-220-142-59.nip.io",
    )
    parser.addini(
        "public_project_path",
        "The path of a public project in the GitLab instance.",
        default="tobiashuste/test-project",
    )
    parser.addini(
        "private_project_path",
        "The path of a private project in the GitLab instance.",
        default="fzo9cf5ccn3/test-e2e",
    )
    parser.addini(
        "private_project_id",
        "The ID of a private project in the GitLab instance.",
        default="109",
    )
    parser.addini(
        "unauthorized_explore_project_slug",
        "The project slug to filter for on the explore page.",
    )
    parser.addini(
        "unauthorized_expected_explore_project_name",
        "The project name expected to be present on the explore page.",
    )
    parser.addini(
        "unauthorized_search_name",
        "The project name to search for.",
    )
    parser.addini(
        "unauthorized_expected_search_project_name",
        "The project name expected to be present on the search results page.",
    )
    parser.addini(
        "gitlab_username",
        "The name of a authenticated GitLab user.",
    )
    parser.addini(
        "aai_username",
        "The username used to authenticate with the Helmholtz AAI.",
    )
    parser.addoption("--gitlab-access-token", action="store", required=True, type=str)
    parser.addoption("--aai-password", action="store", required=True, type=str)
    parser.addoption("--registry-access-token", action="store", required=True, type=str)
    parser.addoption("--package-deploy-token", action="store", required=True, type=str)


def log_in_into_helmholtz_aai(request_config, webdriver_browser: WebDriver):
    """Log in into Helmholtz AAI given credentials and ORCID login method."""
    # set variables used to log in into Helmholtz AAI
    login_method: str = "ORCID"
    aai_username: str = request_config.getini("aai_username")
    aai_password: str = request_config.getoption("aai_password")

    # Find and click on the Helmholtz AAI button in GitLab
    aai_button: WebElement = WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located(
            (
                By.XPATH,
                "//button[@data-testid='oidc-login-button']",
            )
        )
    )
    # Scroll down to the element and click it afterwards
    webdriver_browser.execute_script("arguments[0].scrollIntoView(true);", aai_button)
    aai_button.click()

    # Enter the login method to be used in the search field
    search_field: WebElement = WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located((By.CLASS_NAME, "u-authn-search"))
    )
    search_field.send_keys(login_method)
    search_field.send_keys(Keys.RETURN)

    # Click the ORCID login button
    WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located(
            (
                By.XPATH,
                '//vaadin-button[@class="u-signin-button u-idpAuthentication-oauthWeb.orcid u-text-left"]',
            )
        )
    ).click()

    # Reject cookie banner button
    WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located((By.ID, "onetrust-reject-all-handler"))
    ).click()

    # Enter login credentials
    username_field: WebElement = WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located((By.ID, "username-input"))
    )
    password_field: WebElement = webdriver_browser.find_element(By.ID, "password")

    username_field.send_keys(aai_username)
    password_field.send_keys(aai_password)
    password_field.send_keys(Keys.RETURN)

    # find sign-in button
    WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located((By.ID, "signin-button"))
    ).click()

    # find allow button
    WebDriverWait(webdriver_browser, 20).until(
        ec.presence_of_element_located((By.ID, "IdpButtonsBar.confirmButton"))
    ).click()


def browser_base(chrome_options=webdriver.ChromeOptions()) -> WebDriver:
    """Create the selenium webdriver."""
    driver_url: str = os.environ.get("SELENIUM_DRIVER_URL", "http://localhost:4444")

    driver: WebDriver = webdriver.Remote(
        command_executor=driver_url, options=chrome_options
    )
    driver.maximize_window()
    return driver


@pytest.fixture(scope="function")
def browser() -> WebDriver:
    """Create the selenium webdriver without Chrome options."""
    chrome_options = webdriver.ChromeOptions()
    driver = browser_base(chrome_options=chrome_options)
    yield driver
    driver.quit()


@pytest.fixture(scope="function")
def browser_ignore_cert_errors() -> WebDriver:
    """Create the selenium webdriver ignoring cert errors Chrome options."""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--ignore-certificate-errors")
    driver = browser_base(chrome_options=chrome_options)
    yield driver
    driver.quit()


@pytest.fixture(scope="session", autouse=True)
def config(request):
    """Return a pytest configuration object."""
    return request.config


@pytest.fixture(scope="session", autouse=True)
def credential_callback(config):
    """Return a remote callback object with username-password credentials."""
    username: str = config.getini("gitlab_username")
    password: str = config.getoption("gitlab_access_token")
    credentials = pygit2.UserPass(username, password)
    callback = pygit2.RemoteCallbacks(credentials)
    return callback
