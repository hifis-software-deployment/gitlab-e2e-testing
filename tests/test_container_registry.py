# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test usage of GitLab Container Registry."""

from urllib.parse import urljoin

import docker
import pytest


@pytest.fixture(scope="session")
def client() -> docker.DockerClient:
    """
    Test fixture to provide a Docker Client object.

    Returns:
        docker.DockerClient:
            Docker Client object created from environment variables.
    """
    return docker.from_env()


@pytest.fixture(scope="session")
def tag(config) -> str:
    """
    Test fixture to concatenate registry URL and project name.

    Args:
        config:
            Pytest Config object.

    Returns:
        str:
            Tag as a concatenated string of registry URL and project name.
    """
    return config.getini("registry_url") + "/" + config.getini("private_project_path")


@pytest.fixture(scope="function")
def login(config, client: docker.DockerClient) -> dict:
    """
    Test fixture to log in into GitLab Container Registry.

    Args:
        config:
            Pytest Config object.
        client (docker.DockerClient):
            Docker Client object created from environment variables.

    Returns:
        dict:
            Resulting dictionary from login attempt.
    """
    login_result: dict = client.login(
        username=config.getini("gitlab_username"),
        password=config.getoption("registry_access_token"),
        registry=config.getini("registry_url"),
        reauth=True,
    )
    return login_result


@pytest.fixture(scope="function")
def pushed_image(config, client: docker.DockerClient, login: dict, tag: str) -> str:
    """
    Test fixture to build and push image to GitLab Container Registry.

    Args:
        config:
            Pytest Config object.
        client (docker.DockerClient):
            Docker Client object created from environment variables.
        login (dict):
            Resulting dictionary from login attempt.
        tag (str):
            Tag as a concatenated string of registry URL and project name.

    Yields:
        generator:
            Resulting generator with information about the pushed image.
    """
    dockerfile_path: str = "./tests/"
    build_status: tuple = client.images.build(path=dockerfile_path, tag=tag)
    push_result = client.images.push(repository=tag, stream=False, decode=True)
    yield push_result
    client.images.remove(image=tag)


@pytest.mark.container_registry
def test_container_registry_login_to_gitlab(login: dict) -> None:
    """
    Test logging in into GitLab Container Registry.

    Args:
        login (dict):
            Resulting dictionary from login attempt.
    """
    assert "Login Succeeded" in login["Status"], (
        "Login into Gitlab Container Registry failed: " + login
    )


@pytest.mark.container_registry
def test_container_registry_push_to_gitlab(tag: str, pushed_image) -> None:
    """
    Test pushing image to GitLab Container Registry.

    Args:
        tag (str):
            Tag as a concatenated string of registry URL and project name.
        pushed_image (generator):
            Resulting generator from building and pushing image.
    """
    assert tag in pushed_image, "Docker push into Gitlab Container Registry failed."


@pytest.mark.container_registry
def test_container_registry_pull_from_gitlab(
    client: docker.DockerClient, tag: str, pushed_image
) -> None:
    """
    Test pulling image from GitLab Container Registry.

    Args:
        client (docker.DockerClient):
            Docker Client object created from environment variables.
        tag (str):
            Tag as a concatenated string of registry URL and project name.
        pushed_image (generator):
            Resulting generator from building and pushing image
    """
    tag_version: str = "latest"
    expected_tag_name: str = tag + ":" + tag_version
    image: docker.models.image.Image = client.images.pull(
        repository=tag, tag=tag_version
    )
    assert expected_tag_name in image.attrs["RepoTags"], (
        "Docker pull from Gitlab Container Registry failed: "
        + image.attrs["RepoTags"][0]
    )
