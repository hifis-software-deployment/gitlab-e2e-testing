<!--
SPDX-FileCopyrightText: 2023 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: Apache-2.0
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Group your changes into these categories:

`Added`, `Changed`, `Deprecated`, `Removed`, `Fixed`, `Security`.

## [0.8.2](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.8.2) - 2023-02-15

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.8.1...v0.8.2)

### Changed

- Update dependencies to their latest version

## [0.8.1](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.8.1) - 2023-01-06

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.8.0...v0.8.1)

### Fixed

- Make sure the AAI button is visible in the viewport
  ([!62](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/62)
  by [frust45](https://gitlab.hzdr.de/frust45))

### Changed

- Bump pydocstyle from 6.2.0 to 6.2.2
  ([!61](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/61))
- Bump pydocstyle from 6.1.1 to 6.2.0
  ([!60](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/60))
- Bump isort from 5.11.1 to 5.11.4
  ([!59](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/59))

### Added

- Use Renovate to update images in .gitlab-ci.yml
  ([!63](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/63)
  by [frust45](https://gitlab.hzdr.de/frust45))

## [0.8.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.8.0) - 2022-12-13

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.7.0...v0.8.0)

### Changed

- Update all dependencies to their latest version
  ([!55](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/55)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Fix the E2E tests by using the UFZ test account
  ([!54](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/54)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Bump pytest from 7.1.3 to 7.2.0
  ([!40](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/40)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))

## [0.7.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.7.0) - 2022-10-17

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.6.0...v0.7.0)

### Added

- Use dependabot to update Python dependencies
  ([!31](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/31)
  by [frust45](https://gitlab.hzdr.de/frust45))

### Changed

- Update dependencies in CI and poetry.lock
  ([!38](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/38)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Bump black from 22.6.0 to 22.10.0
  ([!37](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/37)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))
- Bump poetry from 1.1.13 to 1.2.2
  ([!36](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/36)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))
- Bump docker from 5.0.3 to 6.0.0
  ([!35](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/35)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))
- Bump selenium from 4.3.0 to 4.5.0
  ([!33](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/33)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))
- Bump pytest from 7.1.2 to 7.1.3
  ([!32](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/32)
  by [hifis-bot](https://gitlab.hzdr.de/hifis-bot))

## [0.6.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.6.0) - 2022-07-01

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.5.1...v0.6.0)

### Changed

- Update dependencies in GitLab CI and Python
  ([!29](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/29)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Improve caching in GitLab CI
  ([!30](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/30)
  by [frust45](https://gitlab.hzdr.de/frust45))

## [0.5.1](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.5.1) - 2022-07-01

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.5.0...v0.5.1)

### Fixed

- Update GitHub username
  ([!28](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/28)
  by [frust45](https://gitlab.hzdr.de/frust45))

## [0.5.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.5.0) - 2022-05-18

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.4.0...v0.5.0)

### Added

- Add test case to push Python packages to package registry in GitLab
  ([!23](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/23)
  by [hueser93](https://gitlab.hzdr.de/hueser93))

## [0.4.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.4.0) - 2022-05-06

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.3.0...v0.4.0)

### Added

- Add test case for checking GitLab Pages
  ([!24](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/24)
  by [hueser93](https://gitlab.hzdr.de/hueser93))
- Add test case to push and delete remote branches in GitLab
  ([!22](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/22)
  by [hueser93](https://gitlab.hzdr.de/hueser93))

### Fixed

- Develop fixes that repair broken test cases due to changes in UI
  ([!25](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/25)
  by [hueser93](https://gitlab.hzdr.de/hueser93))

## [0.3.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.3.0) - 2022-03-02

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.2.0...v0.3.0)

### Changed

- Cache Python dependencies installed via pip as well
  ([!19](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/19)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Speedup CI pipeline by caching Python dependencies
  ([!18](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/18)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Use Chrome instead of Firefox for testing
  ([!19](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/19)
  by [frust45](https://gitlab.hzdr.de/frust45))

### Fixed

- Fix randomly occuring failure in unauthorized tests
  ([!17](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/17)
  by [frust45](https://gitlab.hzdr.de/frust45))

## [0.2.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.2.0) - 2022-02-17

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/v0.1.0...v0.2.0)

### Added

- Add test for cloning private git repository via https
  ([!10](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/10)
  by [Normo](https://gitlab.hzdr.de/Normo))
- Add test case for the Helmholtz AAI login
  ([!7](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/7)
  by [frust45](https://gitlab.hzdr.de/frust45))
- Add test scenario for testing the GitLab Container Registry
  ([!9](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/9)
  by [hueser93](https://gitlab.hzdr.de/hueser93))

### Changed

- Install the initial stable release of black
  ([!12](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/merge_requests/12)
  by [frust45](https://gitlab.hzdr.de/frust45))

## [0.1.0](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/releases/v0.1.0) - 2022-01-25

[List of commits](https://gitlab.hzdr.de/hifis-software-deployment/gitlab-e2e-testing/-/compare/66b40371e16448130bcc8df799f792f9a8e0ab9a...v0.1.0)

### Added

Initial release of the GitLab End-to-End testing project.
